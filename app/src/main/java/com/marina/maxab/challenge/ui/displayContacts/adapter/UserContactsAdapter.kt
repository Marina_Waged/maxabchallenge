package com.marina.maxab.challenge.ui.displayContacts.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marina.maxab.challenge.R
import com.marina.maxab.challenge.models.UserContact
import kotlinx.android.synthetic.main.item_user_contact.view.*
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Marina Wageed on 28,October,2020
 * Trufla Technology,
 * Cairo, Egypt.
 */


class UserContactsAdapter(private var contactsList : ArrayList<UserContact>, private var listener: IContactClickListener) :
    RecyclerView.Adapter<UserContactsAdapter.UserContactsHolder>()
{

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserContactsHolder
    {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.item_user_contact, parent, false)

        return UserContactsHolder(view)
    }

    override fun getItemCount(): Int
    {
        return contactsList.size
    }

    override fun onBindViewHolder(holder: UserContactsHolder, position: Int)
    {
        val userObject = contactsList[position]

        val upperFirstChar = userObject.userName!!.substring(0,1).toUpperCase(Locale.getDefault())
        holder.contactImage.text = upperFirstChar
        holder.contactName.text = userObject.userName!!
        holder.phoneNumber.text = userObject.phoneNumber!!

        holder.itemLin.setOnClickListener {
            listener.onClickRow(userObject.userName!!, userObject.phoneNumber!!)
        }
    }

    inner class UserContactsHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    {
        val contactImage = itemView.contact_image!!
        val contactName = itemView.contact_name_tv!!
        val phoneNumber = itemView.phone_number_tv!!
        val itemLin = itemView.item_lin!!
    }
}
