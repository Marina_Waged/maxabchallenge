package com.marina.maxab.challenge.ui.displayContacts

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.marina.maxab.challenge.R
import com.marina.maxab.challenge.models.UserContact
import com.marina.maxab.challenge.ui.addContact.AddNewContactActivity
import com.marina.maxab.challenge.ui.contactInfo.ContactInfoActivity
import com.marina.maxab.challenge.ui.displayContacts.adapter.IContactClickListener
import com.marina.maxab.challenge.ui.displayContacts.adapter.UserContactsAdapter
import com.marina.maxab.challenge.utils.PHONE_NUMBER_INTENT_EXTRA
import com.marina.maxab.challenge.utils.USER_NAME_INTENT_EXTRA
import kotlinx.android.synthetic.main.activity_show_contact.*


/**
 * Created by Marina Wageed on 27,October,2020
 * Trufla Technology,
 * Cairo, Egypt.
 */

class ShowContactsActivity : AppCompatActivity(), IContactClickListener
{
    private lateinit var mLayoutManager: LinearLayoutManager
    private lateinit var userContactsAdapter: UserContactsAdapter
    private val myContactsList = ArrayList<UserContact>()

    private lateinit var viewModel: ShowContactViewModel


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_contact)

        this.let {
            viewModel = ViewModelProviders.of(it).get(ShowContactViewModel::class.java)
        }

        viewModel.getContactListMutableLiveData().observe(this, Observer { result -> setUserContactsData(result) })

        onClickAddingBtn()
        initRecyclerView()
    }

    override fun onStart()
    {
        super.onStart()

        clearList()
        showLoading()
        viewModel.getAllContacts()
    }

    private fun onClickAddingBtn()
    {
        add_contact_btn.setOnClickListener {
            startActivity(Intent(this@ShowContactsActivity, AddNewContactActivity::class.java))
        }
    }

    private fun initRecyclerView()
    {
        mLayoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        user_contacts_rv.layoutManager = mLayoutManager

        userContactsAdapter = UserContactsAdapter(myContactsList, this)
        user_contacts_rv.adapter = userContactsAdapter
    }

    private fun showLoading()
    {
        doctor_progress_bar.visibility = View.VISIBLE
    }

    private fun hideLoading()
    {
        doctor_progress_bar.visibility = View.GONE
    }

    private fun setUserContactsData(userContacts : ArrayList<UserContact>)
    {
        hideLoading()
        clearList()

        if(!userContacts.isNullOrEmpty())
            myContactsList.addAll(userContacts)

        else
            empty_list_tv.visibility = View.VISIBLE

        userContactsAdapter.notifyDataSetChanged()
    }

    private fun clearList()
    {
        myContactsList.clear()
        empty_list_tv.visibility = View.GONE
    }

    override fun onClickRow(contactName: String, phoneNumber: String)
    {
        val intent = Intent(this@ShowContactsActivity, ContactInfoActivity::class.java)
        intent.putExtra(USER_NAME_INTENT_EXTRA, contactName)
        intent.putExtra(PHONE_NUMBER_INTENT_EXTRA, phoneNumber)
        startActivity(intent)
    }

}