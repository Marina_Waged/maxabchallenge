package com.marina.maxab.challenge.ui.displayContacts

import android.content.ContentValues.TAG
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.marina.maxab.challenge.base.BaseViewModel
import com.marina.maxab.challenge.models.UserContact
import com.marina.maxab.challenge.utils.PHONE_NUMBER_DB
import com.marina.maxab.challenge.utils.USER_NAME_DB
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


/**
 * Created by Marina Wageed on 27,October,2020
 * Trufla Technology,
 * Cairo, Egypt.
 */

class ShowContactViewModel : BaseViewModel()
{
    private val contactListMutableLiveData = MutableLiveData<ArrayList<UserContact>>()

    fun getContactListMutableLiveData() : MutableLiveData<ArrayList<UserContact>>
    {
        return contactListMutableLiveData
    }

    fun getAllContacts()
    {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                userContactCollection
                    .orderBy(USER_NAME_DB)
                    .get()
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful)
                        {
                            val myContacts : ArrayList<UserContact> = java.util.ArrayList()
                            for (document in task.result!!)
                                myContacts.add(UserContact(document.get(USER_NAME_DB) as String?,
                                    document.get(PHONE_NUMBER_DB) as String?))

                            contactListMutableLiveData.value = myContacts

                        } else {
                            Log.d(TAG, "Error getting documents: ", task.exception)
                        }
                    }

            } catch (cause: Throwable) {
                Log.d("AddNewContactViewModel", cause.localizedMessage!!)
            }

        }
    }

}