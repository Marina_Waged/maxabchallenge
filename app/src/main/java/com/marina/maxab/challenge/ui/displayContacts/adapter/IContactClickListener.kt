package com.marina.maxab.challenge.ui.displayContacts.adapter

/**
 * Created by Marina Wageed on 28,October,2020
 * Trufla Technology,
 * Cairo, Egypt.
 */

interface IContactClickListener
{
    fun onClickRow(contactName : String, phoneNumber : String)
}