package com.marina.maxab.challenge.utils


const val USER_NAME_INTENT_EXTRA = "userName"
const val PHONE_NUMBER_INTENT_EXTRA = "phoneNumber"
const val CALL_PER_INTENT = "tel:"

const val USER_NAME_DB = "userName"
const val PHONE_NUMBER_DB = "phoneNumber"

