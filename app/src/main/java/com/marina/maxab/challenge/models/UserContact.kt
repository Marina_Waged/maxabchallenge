package com.marina.maxab.challenge.models


/**
 * Created by Marina Wageed on 27,October,2020
 * Trufla Technology,
 * Cairo, Egypt.
 */


data class UserContact(var userName: String ?= "",
                       var phoneNumber : String ?= "")
{
}