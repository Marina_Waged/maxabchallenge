package com.marina.maxab.challenge.ui.contactInfo

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.marina.maxab.challenge.base.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Created by Marina Wageed on 28,October,2020
 * Trufla Technology,
 * Cairo, Egypt.
 */
class ContactInfoViewModel : BaseViewModel()
{
    private val deleteContactStatus = MutableLiveData<Boolean>()


    fun getDeleteContactStatus() : MutableLiveData<Boolean>
    {
        return deleteContactStatus
    }

    fun deleteContact(phoneNumber : String)
    {
        GlobalScope.launch(Dispatchers.IO) {
            try {
                userContactCollection.document(phoneNumber).delete()
                    .addOnCompleteListener { task ->
                        deleteContactStatus.value = task.isSuccessful
                    }

            } catch (cause: Throwable) {
                Log.d("AddNewContactViewModel", cause.localizedMessage!!)
            }

        }
    }
}