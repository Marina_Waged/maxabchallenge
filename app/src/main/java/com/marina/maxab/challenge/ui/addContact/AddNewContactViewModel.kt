package com.marina.maxab.challenge.ui.addContact

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.marina.maxab.challenge.base.BaseViewModel
import com.marina.maxab.challenge.models.UserContact
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

/**
 * Created by Marina Wageed on 27,October,2020
 * Trufla Technology,
 * Cairo, Egypt.
 */

class AddNewContactViewModel : BaseViewModel()
{
    private val contactAddedStatus = MutableLiveData<Boolean>()

    fun getContactAddedStatus() : MutableLiveData<Boolean>
    {
        return contactAddedStatus
    }

    fun saveNewContact(userName : String?, phoneNumber : String?)
    {
        val userContact = UserContact(userName!!, phoneNumber!!)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                userContactCollection.document(phoneNumber)
                    .set(userContact).await()
                withContext(Dispatchers.Main)
                {
                    contactAddedStatus.value = true
                }

            }catch (cause: Throwable)
            {
               Log.d("AddNewContactViewModel", cause.localizedMessage!!)
            }

        }

    }

}