package com.marina.maxab.challenge.ui.contactInfo

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.marina.maxab.challenge.R
import com.marina.maxab.challenge.utils.CALL_PER_INTENT
import com.marina.maxab.challenge.utils.PHONE_NUMBER_INTENT_EXTRA
import com.marina.maxab.challenge.utils.ProgressDialog
import com.marina.maxab.challenge.utils.USER_NAME_INTENT_EXTRA
import kotlinx.android.synthetic.main.activity_contact_info.*


/**
 * Created by Marina Wageed on 27,October,2020
 * Trufla Technology,
 * Cairo, Egypt.
 */

class ContactInfoActivity :  AppCompatActivity()
{
    private var contactName : String ?= null
    private var phoneNumber : String ?= null

    private lateinit var viewModel: ContactInfoViewModel
    private val callPermeationCode = 1888
    private lateinit var dialog : Dialog


    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_info)

        this.let {
            viewModel = ViewModelProviders.of(it).get(ContactInfoViewModel::class.java)
        }

        dialog = ProgressDialog.progressDialog(this)

        viewModel.getDeleteContactStatus().observe(this, Observer { result ->
            if(result)
                deletedSuccess()})

        contact_info_back_btn.setOnClickListener {
            finish()
        }

        initView()
        callPhoneNumber()
        onClickDelete()
    }

    private fun initView()
    {
        //Receive Intent data
        contactName  = intent?.getStringExtra(USER_NAME_INTENT_EXTRA)!!
        phoneNumber = intent?.getStringExtra(PHONE_NUMBER_INTENT_EXTRA)!!

        //Set data to view
        contact_info_name_tv.text = contactName!!
        add_contact_phone_tv.text = phoneNumber!!
    }

    private fun callPhoneNumber()
    {
        contact_info_call_lin.setOnClickListener {
            checkPermission()
        }
    }

    private fun checkPermission() : Boolean
    {
        var result = false

        if (Build.VERSION.SDK_INT >= 23)
        {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED)
                return true
            else
            {
                val permissionRequest = arrayOf(Manifest.permission.CALL_PHONE)
                requestPermissions(permissionRequest, callPermeationCode)
            }
        } else
            result = true

        return result
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray)
    {
        if (requestCode == callPermeationCode)
        {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                if(!phoneNumber.isNullOrEmpty())
                {
                    val intent = Intent(Intent.ACTION_CALL)
                    intent.data = Uri.parse(CALL_PER_INTENT + phoneNumber)
                    startActivity(intent)
                }

            } else
                Toast.makeText(this, "Call Permission Denied", Toast.LENGTH_LONG).show()
            return
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun showLoading()
    {
        dialog.show()
    }

    private fun hideLoading()
    {
        dialog.dismiss()
    }

    private fun onClickDelete()
    {
        contact_info_delete_btn.setOnClickListener {
            showLoading()
            viewModel.deleteContact(phoneNumber!!)
        }
    }

    private fun deletedSuccess()
    {
        hideLoading()
        finish()
    }
}