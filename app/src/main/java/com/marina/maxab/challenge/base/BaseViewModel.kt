package com.marina.maxab.challenge.base

import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


/**
 * Created by Marina Wageed on 28,October,2020
 * Trufla Technology,
 * Cairo, Egypt.
 */

open class BaseViewModel : ViewModel()
{
    val userContactCollection = Firebase.firestore.collection("user_contact")
}