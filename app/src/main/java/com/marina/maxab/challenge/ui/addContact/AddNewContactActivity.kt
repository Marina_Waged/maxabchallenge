package com.marina.maxab.challenge.ui.addContact

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.marina.maxab.challenge.R
import com.marina.maxab.challenge.ui.contactInfo.ContactInfoActivity
import com.marina.maxab.challenge.utils.PHONE_NUMBER_INTENT_EXTRA
import com.marina.maxab.challenge.utils.ProgressDialog
import com.marina.maxab.challenge.utils.USER_NAME_INTENT_EXTRA
import kotlinx.android.synthetic.main.activity_add_new_contact.*


/**
 * Created by Marina Wageed on 27,October,2020
 * Trufla Technology,
 * Cairo, Egypt.
 */

class AddNewContactActivity : AppCompatActivity()
{
    private lateinit var viewModel: AddNewContactViewModel
    private var userName : String = ""
    private var phoneNumber : String = ""
    private lateinit var dialog : Dialog

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_contact)

        this.let {
            viewModel = ViewModelProviders.of(it).get(AddNewContactViewModel::class.java)
        }

        dialog = ProgressDialog.progressDialog(this)

        viewModel.getContactAddedStatus().observe(this, Observer { result ->
            if(result)
                isAddedSuccess()
            else
                displayErrorMsg()
        })

        onClickSave()
        onClickClose()
    }

    private fun onClickSave()
    {
        add_contact_save_tv.setOnClickListener {
            userName = add_contact_name_et.text.toString().trim()
            phoneNumber = add_contact_phone_et.text.toString().trim()

            if(userName.isNotEmpty() && phoneNumber.isNotEmpty())
            {
                showLoading()
                viewModel.saveNewContact(userName, phoneNumber)

            }else
            {
                add_contact_name_et.error = getString(R.string.please_enter_name)
                add_contact_phone_et.error = getString(R.string.please_enter_phone)
            }
        }
    }

    private fun showLoading()
    {
        dialog.show()
    }

    private fun hideLoading()
    {
        dialog.dismiss()
    }

    private fun isAddedSuccess()
    {
        hideLoading()
        val intent = Intent(this@AddNewContactActivity, ContactInfoActivity::class.java)
        intent.putExtra(USER_NAME_INTENT_EXTRA, userName)
        intent.putExtra(PHONE_NUMBER_INTENT_EXTRA, phoneNumber)
        startActivity(intent)
        finish()
    }

    private fun displayErrorMsg()
    {
        Toast.makeText(this, getString(R.string.some_thing_wrong), Toast.LENGTH_LONG).show()
    }

    private fun onClickClose()
    {
        add_contact_close_btn.setOnClickListener {
            finish()
        }
    }
}